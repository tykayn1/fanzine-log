<?php

namespace AppBundle\Traits;

trait Sellable {

	/**
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	private $price;

	/**
	 * @return mixed
	 */
	public function getPrice() {
		return $this->price;
	}

	/**
	 * @param mixed $price
	 */
	public function setPrice( $price ) {
		$this->price = $price;
	}
}
