<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Product controller.
 *
 * @Route("product")
 */
class ProductController extends Controller {
	/**
	 * Lists all product entities.
	 *
	 * @Route("/", name="product_index")
	 * @Method("GET")
	 */
	public function indexAction() {

		$products = $this->getUser()->getProducts();

		return $this->render( 'product/index.html.twig',
			[
				'products' => $products,
			] );
	}

	/**
	 * Creates a new product entity.
	 *
	 * @Route("/new", name="product_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction( Request $request ) {
		$user    = $this->getUser();
		$product = new Product();
		$product->setUser( $user );
		$product->setPrice( 1 );
		$product->setStockCount( 500 );
		$form = $this->createForm( 'AppBundle\Form\ProductType', $product );
		$form->handleRequest( $request );
		$user = $this->getUser();
		if ( $user && $user->getCategories() ) {
			$product->setCategory( $user->getCategories()[ 0 ] );
		}
		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->persist( $product );
			$em->flush();

			return $this->redirectToRoute( 'product_show', [ 'id' => $product->getId() ] );
		}

		return $this->render( 'product/new.html.twig',
			[
				'product' => $product,
				'form'    => $form->createView(),
			] );
	}

	/**
	 * Finds and displays a product entity.
	 *
	 * @Route("/{id}", name="product_show")
	 * @Method("GET")
	 */
	public function showAction( Product $product ) {
		if ( $product->getUser()->getId() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$deleteForm = $this->createDeleteForm( $product );
		if ( $product->getUser()->getId() !== $this->getUser()->getId() ) {

			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}

		return $this->render( 'product/show.html.twig',
			[
				'product'     => $product,
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Displays a form to edit an existing product entity.
	 *
	 * @Route("/{id}/edit", name="product_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction( Request $request, Product $product ) {

		if ( $product->getUser()->getId() !== $this->getUser()->getId() ) {

			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}

		$deleteForm = $this->createDeleteForm( $product );
		$editForm   = $this->createForm( 'AppBundle\Form\ProductType', $product );
		$editForm->handleRequest( $request );

		if ( $editForm->isSubmitted() && $editForm->isValid() ) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute( 'product_edit', [ 'id' => $product->getId() ] );
		}

		return $this->render( 'product/edit.html.twig',
			[
				'product'     => $product,
				'edit_form'   => $editForm->createView(),
				'delete_form' => $deleteForm->createView(),
			] );
	}

	/**
	 * Deletes a product entity.
	 *
	 * @Route("/{id}", name="product_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction( Request $request, Product $product ) {
		if ( $product->getUser()->getId() !== $this->getUser()->getId() ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$form = $this->createDeleteForm( $product );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			if ( $product->getUser()->getId() !== $this->getUser()->getId() ) {

				$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
			}
			$em = $this->getDoctrine()->getManager();
			$em->remove( $product );
			$em->flush();
		}

		return $this->redirectToRoute( 'product_index' );
	}

	/**
	 * Creates a form to delete a product entity.
	 *
	 * @param Product $product The product entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm( Product $product ) {
		return $this->createFormBuilder()
		            ->setAction( $this->generateUrl( 'product_delete', [ 'id' => $product->getId() ] ) )
		            ->setMethod( 'DELETE' )
		            ->getForm();
	}
}
