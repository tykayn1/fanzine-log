<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ProductCategory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Productcategory controller.
 *
 * @Route("productcategory")
 */
class ProductCategoryController extends Controller {
	/**
	 * Lists all productCategory entities.
	 *
	 * @Route("/", name="productcategory_index")
	 * @Method("GET")
	 */
	public function indexAction() {

		$currentUser       = $this->getUser();
		$productCategories = $currentUser->getCategories();

		return $this->render( 'productcategory/index.html.twig',
			[
				'productCategories' => $productCategories,
			] );
	}

	/**
	 * Creates a new productCategory entity.
	 *
	 * @Route("/new", name="productcategory_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction( Request $request ) {
		$productCategory = new Productcategory();
		$currentUser     = $this->getUser();
		$currentUser->addCategory( $productCategory );
		$productCategory->setUsers( [ $currentUser ] );
		$form = $this->createForm( 'AppBundle\Form\ProductCategoryType', $productCategory );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->persist( $productCategory );
			$em->persist( $currentUser );
			$em->flush();

			return $this->redirectToRoute( 'productcategory_index' );
		}

		return $this->render( 'productcategory/new.html.twig',
			[
				'productCategory' => $productCategory,
				'form'            => $form->createView(),
			] );
	}

	/**
	 * Finds and displays a productCategory entity.
	 *
	 * @Route("/{id}", name="productcategory_show")
	 * @Method("GET")
	 */
	public function showAction( ProductCategory $productCategory ) {
        if ( $productCategory->getUser()->getId() !== $this->getUser()->getId() ) {
            $this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
        }
		$deleteForm = $this->createDeleteForm( $productCategory );

		return $this->render( 'productcategory/show.html.twig',
			[
				'productCategory' => $productCategory,
				'delete_form'     => $deleteForm->createView(),
			] );
	}

	/**
	 * Displays a form to edit an existing productCategory entity.
	 *
	 * @Route("/{id}/edit", name="productcategory_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction( Request $request, ProductCategory $productCategory ) {
		if ( ! $productCategory->hasUser( $this->getUser()->getId() ) ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}

		$deleteForm  = $this->createDeleteForm( $productCategory );
		$currentUser = $this->getUser();
		$productCategory->setUsers( [ $currentUser ] );
		$editForm = $this->createForm( 'AppBundle\Form\ProductCategoryType', $productCategory );
		$editForm->handleRequest( $request );

		if ( $editForm->isSubmitted() && $editForm->isValid() ) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute( 'productcategory_edit', [ 'id' => $productCategory->getId() ] );
		}

		return $this->render( 'productcategory/edit.html.twig',
			[
				'productCategory' => $productCategory,
				'edit_form'       => $editForm->createView(),
				'delete_form'     => $deleteForm->createView(),
			] );
	}

	/**
	 * Deletes a productCategory entity.
	 *
	 * @Route("/{id}", name="productcategory_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction( Request $request, ProductCategory $productCategory ) {
		if ( $productCategory->hasUser( $this->getUser()->getId() ) ) {
			$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
		}
		$form = $this->createDeleteForm( $productCategory );
		$form->handleRequest( $request );

		if ( $form->isSubmitted() && $form->isValid() ) {
			if ( ! $productCategory->hasUser( $this->getUser()->getId() ) ) {
				$this->denyAccessUnlessGranted( 'ROLE_ADMIN' );
			}
			$em = $this->getDoctrine()->getManager();
			$em->remove( $productCategory );
			$em->flush();
		}

		return $this->redirectToRoute( 'productcategory_index' );
	}

	/**
	 * Creates a form to delete a productCategory entity.
	 *
	 * @param ProductCategory $productCategory The productCategory entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm( ProductCategory $productCategory ) {
		return $this->createFormBuilder()
		            ->setAction( $this->generateUrl( 'productcategory_delete', [ 'id' => $productCategory->getId() ] ) )
		            ->setMethod( 'DELETE' )
		            ->getForm();
	}
}
