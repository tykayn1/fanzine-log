<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="custom_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser {
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="twitter_id", type="string", length=255, nullable=true)
	 */
	private $twitterId;
	private $twitterAccessToken;
	/**
	 * @ORM\Column(name="disqus_id", type="string", length=255, nullable=true)
	 */
	private $disqusId;
	private $disqusAccessToken;
	/**
	 * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
	 */
	private $googleId;

	private $googleAccessToken;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\ProductCategory", inversedBy="users")
     */
    private $categories;
	/**
	 * templates products
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Product", mappedBy="user")
	 */
	private $products;

	/**
	 * variabilised products sold
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductSold", mappedBy="user")
	 */
	private $productsSold;
	/**
	 * variabilised products sold
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Festival", mappedBy="user")
	 */
	private $festivals;
	/**
	 * series of festivals
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\SerieFestival", mappedBy="user")
	 */
	private $seriesFestivals;

    /**
     * current festival we are recording sellings for
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Festival")
     */
    private $activeFestival;

    //expenses previsionnel configs
    /**
     * @ORM\Column(name="averageMonthlyEarnings", type="float", nullable=true)
     */
    private $averageMonthlyEarnings;

    /**
     * @ORM\Column(name="disponibility", type="float", nullable=true)
     */
    private $disponibility;
	/**
	 * variabilised products sold
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\ExpenseKind", mappedBy="user")
	 */
	private $expenses;

    /**
     * @return mixed
     */
    public function getAverageMonthlyEarnings()
    {
        return $this->averageMonthlyEarnings;
    }

    /**
     * @param mixed $averageMonthlyEarnings
     */
    public function setAverageMonthlyEarnings($averageMonthlyEarnings)
    {
        $this->averageMonthlyEarnings = $averageMonthlyEarnings;
    }
    /**
     * @return mixed
     */
    public function getDisponibility()
    {
        return $this->disponibility;
    }

    /**
     * @param mixed $disponibility
     */
    public function setDisponibility($disponibility)
    {
        $this->disponibility = $disponibility;
    }

    /**
     * @return mixed
     */
    public function getSeriesFestivals()
    {
        return $this->seriesFestivals;
    }

    /**
     * @param mixed $seriesFestivals
     */
    public function setSeriesFestivals($seriesFestivals)
    {
        $this->seriesFestivals = $seriesFestivals;
    }
    /**
     * @return mixed
     */
    public function getExpenses()
    {
        return $this->expenses;
    }

    /**
     * @param mixed $expenses
     */
    public function setExpenses($expenses)
    {
        $this->expenses = $expenses;
    }

	/**
	 * @return mixed
	 */
	public function getProductsSold() {
		return $this->productsSold;
	}

	/**
	 * @return mixed
	 */
	public function addProductsSold( $product ) {
		return $this->productsSold[] = $product;
	}

	/**
	 * @param mixed $productsSold
	 */
	public function setProductsSold( $productsSold ) {
		$this->productsSold = $productsSold;
	}

	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\SellRecord", mappedBy="user")
	 */
	private $sellRecords;

	/**
	 * @return mixed
	 */
	public function getSellRecords() {
		return $this->sellRecords;
	}

	/**
	 * @param mixed $sellRecords
	 */
	public function setSellRecords( $sellRecords ) {
		$this->sellRecords = $sellRecords;
	}

	/**
	 * @param mixed $sellRecords
	 */
	public function addSellRecords( $sellRecords ) {
		$this->sellRecords[] = $sellRecords;
	}

	/**
	 * @return mixed
	 */
	public function getGoogleAccessToken() {
		return $this->googleAccessToken;
	}

	/**
	 * @param mixed $googleAccessToken
	 */
	public function setGoogleAccessToken( $googleAccessToken ) {
		$this->googleAccessToken = $googleAccessToken;
	}

	/**
	 * @return mixed
	 */
	public function getDisqusAccessToken() {
		return $this->disqusAccessToken;
	}

	/**
	 * @param mixed $disqusAccessToken
	 */
	public function setDisqusAccessToken( $disqusAccessToken ) {
		$this->disqusAccessToken = $disqusAccessToken;
	}

	/**
	 * @return mixed
	 */
	public function getTwitterAccessToken() {
		return $this->twitterAccessToken;
	}

	/**
	 * @param mixed $twitterAccessToken
	 */
	public function setTwitterAccessToken( $TwitterAccessToken ) {
		$this->twitterAccessToken = $TwitterAccessToken;
	}

	/**
	 * @return mixed
	 */
	public function getProducts() {
		return $this->products;
	}

	/**
	 * @param mixed $products
	 */
	public function setProducts( $products ) {
		$this->products = $products;
	}

	/**
	 * @return mixed
	 */
	public function getCategories() {
		return $this->categories;
	}

	/**
	 * @param mixed $categories
	 */
	public function setCategories( $categories ) {
		$this->categories = $categories;
	}

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set googleId
	 *
	 * @param string $googleId
	 *
	 * @return User
	 */
	public function setGoogleId( $googleId ) {
		$this->googleId = $googleId;

		return $this;
	}

	/**
	 * Get googleId
	 *
	 * @return string
	 */
	public function getGoogleId() {
		return $this->googleId;
	}

	/**
	 * Add product
	 *
	 * @param \AppBundle\Entity\Product $product
	 *
	 * @return User
	 */
	public function addProduct( \AppBundle\Entity\Product $product ) {
		$this->products[] = $product;

		return $this;
	}

	/**
	 * Remove product
	 *
	 * @param \AppBundle\Entity\Product $product
	 */
	public function removeProduct( \AppBundle\Entity\Product $product ) {
		$this->products->removeElement( $product );
	}

	/**
	 * Remove productsSold
	 *
	 * @param \AppBundle\Entity\ProductSold $productsSold
	 */
	public function removeProductsSold( \AppBundle\Entity\ProductSold $productsSold ) {
		$this->productsSold->removeElement( $productsSold );
	}

	/**
	 * Add category
	 *
	 * @param \AppBundle\Entity\ProductCategory $category
	 *
	 * @return User
	 */
	public function addCategory( \AppBundle\Entity\ProductCategory $category ) {
		$this->categories[] = $category;

		return $this;
	}

	/**
	 * Remove category
	 *
	 * @param \AppBundle\Entity\ProductCategory $category
	 */
	public function removeCategory( \AppBundle\Entity\ProductCategory $category ) {
		$this->categories->removeElement( $category );
	}

	/**
	 * Add sellRecord
	 *
	 * @param \AppBundle\Entity\SellRecord $sellRecord
	 *
	 * @return User
	 */
	public function addSellRecord( \AppBundle\Entity\SellRecord $sellRecord ) {
		$this->sellRecords[] = $sellRecord;

		return $this;
	}

	/**
	 * Remove sellRecord
	 *
	 * @param \AppBundle\Entity\SellRecord $sellRecord
	 */
	public function removeSellRecord( \AppBundle\Entity\SellRecord $sellRecord ) {
		$this->sellRecords->removeElement( $sellRecord );
	}

	/**
	 * Add festival.
	 *
	 * @param \AppBundle\Entity\Festival $festival
	 *
	 * @return User
	 */
	public function addFestival( \AppBundle\Entity\Festival $festival ) {
		$this->festivals[] = $festival;

		return $this;
	}

	/**
	 * Remove festival.
	 *
	 * @param \AppBundle\Entity\Festival $festival
	 *
	 * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
	 */
	public function removeFestival( \AppBundle\Entity\Festival $festival ) {
		return $this->festivals->removeElement( $festival );
	}

	/**
	 * Get festivals.
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getFestivals() {
		return $this->festivals;
	}

	/**
	 * Set activeFestival.
	 *
	 * @param \AppBundle\Entity\Festival|null $activeFestival
	 *
	 * @return User
	 */
	public function setActiveFestival( \AppBundle\Entity\Festival $activeFestival = null ) {
		$this->activeFestival = $activeFestival;

		return $this;
	}

	/**
	 * Get activeFestival.
	 *
	 * @return \AppBundle\Entity\Festival|null
	 */
	public function getActiveFestival() {
		return $this->activeFestival;
	}

	/**
	 * Set twitterId.
	 *
	 * @param string|null $twitterId
	 *
	 * @return User
	 */
	public function setTwitterId( $twitterId = null ) {
		$this->twitterId = $twitterId;

		return $this;
	}

	/**
	 * Get twitterId.
	 *
	 * @return string|null
	 */
	public function getTwitterId() {
		return $this->twitterId;
	}

	/**
	 * Set disqusId.
	 *
	 * @param string|null $disqusId
	 *
	 * @return User
	 */
	public function setDisqusId( $disqusId = null ) {
		$this->disqusId = $disqusId;

		return $this;
	}

	/**
	 * Get disqusId.
	 *
	 * @return string|null
	 */
	public function getDisqusId() {
		return $this->disqusId;
	}
}
