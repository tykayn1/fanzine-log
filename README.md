Blueprint Cipherbliss 
====

A Symfony project created on March 23, 2018, 2:09 pm.

Features:
bootstrap, font awesome, user bundle

install dependencies: 
```bash
bash install.sh
```
edit parameters.yml to link to your mysql server

update schema with doctrine
 ```bash
php bin/console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force
```
compile front end assets
```bash
yarn run encore dev --watch
```

access the front with your browser
```bash
php bin/console server:run
firefox http://http://127.0.0.1:8000/
```
enjoy!

www.cipherbliss.com
